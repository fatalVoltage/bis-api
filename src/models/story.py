from src.index import db


class Story(db.Model):
    __tablename__ = "stories"

    id = db.Column("id", db.String, primary_key=True)
    links = db.Column("links", db.JSON)
    created_at = db.Column("created_at", db.Date)

    def __init__(self, id, source, links, created_at):
        self.id = id
        self.source = source
        self.links = links
        self.created_at = created_at
