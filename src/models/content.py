from src.index import db


class Content(db.Model):
    __tablename__ = "content"

    id = db.Column("id", db.String, primary_key=True)
    page = db.Column("page", db.String)
    content = db.Column("content", db.JSON)
    created_at = db.Column("created_at", db.Date)
    updated_at = db.Column("updated_at", db.Date)

    def __init__(self, id, page, content, created_at, updated_at):
        self.id = id
        self.page = page
        self.content = content
