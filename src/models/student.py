from src.index import db


class Student(db.Model):
    __tablename__ = "students"

    id = db.Column("id", db.String, primary_key=True)
    name = db.Column("name", db.String)
    address = db.Column("address", db.String)
    guardian = db.Column("guardian", db.String)
    contact = db.Column("contact", db.String)
    guardian_contact = db.Column("guardian_contact", db.String)
    s_class = db.Column("s_class", db.String)
    created_at = db.Column("created_at", db.Date)

    def __init__(self, id, name, address, guardian, contact, guardian_contact, s_class, created_at):
        self.id = id
        self.name = name
        self.address = address
        self.guardian = guardian
        self.contact = contact
        self.guardian_contact = guardian_contact
        self.s_class = s_class
        self.created_at = created_at
