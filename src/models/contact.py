from src.index import db


class Contact(db.Model):
    __tablename__ = "contact"

    id = db.Column("id", db.String, primary_key=True)
    email = db.Column("email", db.String)
    phone_number = db.Column("phone_number", db.String)
    name = db.Column("name", db.String)
    created_at = db.Column("created_at", db.Date)
    done = db.Column("done", db.Boolean)

    def __init__(self, id, email, phone_number, name, created_at, done):
        self.id = id
        self.email = email
        self.phone_number = phone_number
        self.name = name
        self.created_at = created_at
        self.done = done
