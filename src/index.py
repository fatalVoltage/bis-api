from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy
from uuid import uuid4
import datetime


app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "postgresql://postgres:123456@127.0.0.1:5432/postgres"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)


from .models.contact import Contact
from .models.content import Content
from .models.student import Student
from .models.story import Story


@app.route("/get-content/<string:page>", methods=["GET"])
def get_page_content(page):
    content = Content.query.filter_by(page=page).first()
    mapped_content = {
        "page": content.page,
        "content": content.content
    }
    return jsonify(mapped_content)


@app.route("/add-content", methods=["POST"])
def add_content():
    c_data = request.get_json()
    content = Content(
        uuid4(),
        c_data["page"],
        c_data["content"],
        datetime.datetime.now(),
        None)
    return jsonify({"status": "success"})


@app.route("/contact", methods=["POST"])
def request_contact():
    c_data = request.get_json()
    try:
        contact = Contact(
            id=uuid4(),
            email=c_data["email"],
            phone_number=c_data["phone_number"],
            name=c_data["name"],
            created_at=datetime.datetime.now(),
            done=False)
        db.session.add(contact)
        db.session.commit()
        return jsonify({"status": "success"})
    except Exception as e:
        print(e)
        return jsonify({"status": "error"})


@app.route("/get-contact-requests", methods=["GET"])
def get_contact_requests():
    contact_requests = Contact.query.filter_by(done=False).all()
    mapped_contact_requests = []
    for c in contact_requests:
        cr = {
            "id": c.id,
            "email": c.email,
            "phone_number": c.phone_number,
            "name": c.name,
            "created_at": c.created_at,
            "done": c.done
        }
        mapped_contact_requests.append(cr)
    return jsonify(mapped_contact_requests)


@app.route("/student/new", methods=["POST"])
def new_student():
    s_data = request.get_json()
    try:
        student = Student(
            uuid4(),
            name=s_data["name"],
            address=s_data["address"],
            guardian=s_data["guardian"],
            contact=s_data["contact"],
            g_contact=s_data["g_contact"],
            s_class=s_data["s_class"],
            created_at=datetime.datetime.now())
        db.session.add(student)
        db.session.commit()
        return jsonify({"status": "success"})
    except:
        return jsonify({"status": "error"})
